"1 - Hvem er jeg og hva er navnet paa dette scriptet?"
"2 - Hvor lenge er det siden siste boot?"
"3 - Hvor mange prosesser og traader finnes?"
"4 - Hvor mange context switch'er fant sted siste sekund?"
"5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?"
"6 - Hvor mange interrupts fant sted siste sekund?"
"9 - Avslutt dette scriptet"

$whoami = whoami
$lastboot = (get-date) - (Get-CimInstance Win32_OperatingSystem).LastBootUpTime
$pro = (get-process).count
$thread = (Get-ciminstance win32_thread).count
[int]$conswitch = $($(Get-Counter -Counter "\System\Context Switches/sec").CounterSamples | Format-Table CookedValue -HideTableHeaders -autosize | Out-String)
[int]$conswitch = $conswitch -replace "`n|",""
$user=$($(Get-Counter -Counter "\Processor(_total)\% User Time" -SampleInterval 1 -MaxSamples 1).CounterSamples | format-table CookedValue -AutoSize -HideTableHeaders | Out-String)
$user = $user -replace "`n|",""
$kernel=$($(Get-Counter -Counter "\Processor(_total)\% Privileged Time" -SampleInterval 1 -MaxSamples 1).CounterSamples | format-table CookedValue -AutoSize -HideTableHeaders | Out-String)
$kernel = $kernel -replace "`n|",""
$inter=$($(Get-Counter -Counter "\Processor(_total)\Interrupts/sec").CounterSamples | Format-Table CookedValue  -AutoSize -hidetableheaders | Out-String)
$inter= $inter -replace "\n|",""


$choice = Read-Host

switch ( $choice )
{
	1 { "Dette er " + $whoami + " og dette er oblig3-1.ps1"; break}
	2 { "Det er " + $lastboot.Hours + "timer " + $lastboot.Minutes + "minutter " + $lastboot.Seconds + "sekunder siden siste boot"; break}
	3 { "Det finnes " + $pro + " prosesser og " + $thread + " traader"; break}
	4 { "Det var " + $conswitch + " context switcher det siste sekundet"; break}
	5 { "$kernel % kernel og $user % user"; break}
	6 { "Det var " + $inter + " antall interrupts siste sekund"; break }
	9 { break }
}