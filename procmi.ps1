﻿function readable($num) {
    if ( $num -lt 1024 ) { Write-Output "$($num)B" }
    elseif ( $num -lt 1024*1024 ) { Write-Output "$(($num/1024))KB" }
    elseif ( $num -lt 1024*1024*1024 ) { Write-Output "$(($num/(1024*1024)))MB" }
    elseif ( $num -lt 1024*1024*1024*1024 ) { Write-Output "$(($num/(1024*1024*1024)))GB" }
    elseif ( $num -lt 1024*1024*1024*1024*1024 ) { Write-Output "$(($num/(1024*1024*1024*1024)))TB" }
}

$tid=$(Get-Date -UFormat "%Y%m%d-%H%M%S")

foreach($i in $args){
  $prosess=$(Get-Process | Where-Object{$_.Id -eq $i})
  $virmem=$(readable($prosess.VirtualMemorySize64))
  $workspace=$(readable($prosess.WorkingSet64))
  New-Item ".\$i--$tid.meminfo" -ItemType file -Force -Value "Minne info om prosess med PID $i `n Total bruk av virtuelt minne: $virmem `n St`ørrelse p`å Working Set: $workspace"
}